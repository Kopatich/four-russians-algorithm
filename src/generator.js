/**
 * Generates random boolean matrix with n &times; n size
 * @param {number} n Size of generated matrix
 * @returns Random boolean matrix
 */

function generate(n) {
  const matrix = [];
  for (let i = 0; i < n; i++) {
    const line = [];
    for (let j = 0; j < n; j++) {
      line.push(Math.round(Math.random()));
    }
    matrix.push(line);
  }
  return matrix;
}

module.exports = {
  generate,
};
