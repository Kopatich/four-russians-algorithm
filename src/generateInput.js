const fs = require("fs");
const path = require("path");
const { generate } = require("./generator");

const indexes = [4, 5, 10, 23, 115, 1091];
const input = [];

for (let i of indexes) {
  const A = generate(i);
  const B = generate(i);
  input.push([A, B]);
}

fs.writeFileSync(
  path.join(__dirname, "../data/input1.json"),
  JSON.stringify({ input })
);
