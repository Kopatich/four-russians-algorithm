/**
 *  Method which implements Four Russians' Algorithm
 * @param {number[]} A Boolean matrix n &times; n
 * @param {number[]} B Boolean matrix n &times; n
 *
 * @returns {number[]} Matrix C = AB
 */

function FRA(A, B) {
  A = preprocessMatrix(A, false);
  B = preprocessMatrix(B, true);

  C = [];

  for (let i = 0; i < A.length; i++) {
    line = [];
    for (let j = 0; j < A.length; j++) {
      line.push(scalarBinary(A[i], B[j]));
    }
    C.push(line);
  }

  return C;
}

/**
 *
 * @param {number[]} A Matrix to preprocess
 * @param {boolean} onColumns Direction to preprocess, columns by default
 */

function preprocessMatrix(A, onColumns = true) {
  const n = A.length;
  const k = Math.floor(Math.log2(n));

  const ret = [];

  A = onColumns ? transpose(A) : A;

  for (let row = 0; row < n; row++) {
    let line = [];
    for (let i = 0; i < Math.ceil(n / k); i++) {
      let int = "";
      for (let j = 0; j < k; j++) {
        let number = 2 * i + j < n ? A[row][2 * i + j] : 0;
        int = `${int}${number}`;
      }
      line.push(parseInt(int, 2));
    }
    ret.push(line);
  }
  return ret;
}

/**
 * Function to get transposed matrix
 * @param {number[][]} matrix Matrix to transpose
 * @returns Transposed matrix
 */

function transpose(matrix) {
  let [row] = matrix;
  return row.map((value, column) => matrix.map((row) => row[column]));
}

/**
 * Function to get binary scalar multiplication of two binary (1 &times; n) vectors
 * @param {number} a Vector 1 &times; n
 * @param {number} b Vector 1 &times; n
 * @returns Scalar multiplication of 2 binary vectors
 */

function scalarBinary(a, b) {
  let sum = 0;
  for (let i = 0; i < a.length; i++) {
    const tmp = (a[i] & b[i]).toString(2).split("");
    const tmp_sum = tmp.reduce((acc, cur) => acc + Number(cur), 0);
    sum += tmp_sum;
  }
  return sum % 2;
}

module.exports = {
  FRA,
};
