const { FRA } = require("../src/index");
const { input } = require("../data/input.json");
const { output } = require("../data/output.json");

const assert = require("assert").strict;

/**
 * Constant that enlarges overload test (1091 matrix size)
 */
const OVERLOAD_TIMEOUT = 1000 * 60 * 2; //ms

describe("Four russians algorithm test", function () {
  it("Matrix's size = 4", function () {
    const [A, B] = input[0];
    const C = output[0];
    assert.equal(JSON.stringify(FRA(A, B)), JSON.stringify(C));
  });

  it("Matrix's size = 5", function () {
    const [A, B] = input[1];
    const C = output[1];
    assert.equal(JSON.stringify(FRA(A, B)), JSON.stringify(C));
  });

  it("Matrix's size = 10", function () {
    const [A, B] = input[2];
    const C = output[2];
    assert.equal(JSON.stringify(FRA(A, B)), JSON.stringify(C));
  });

  it("Matrix's size = 23", function () {
    const [A, B] = input[3];
    const C = output[3];
    assert.equal(JSON.stringify(FRA(A, B)), JSON.stringify(C));
  });

  it("Matrix's size = 115", function () {
    const [A, B] = input[4];
    const C = output[4];
    assert.equal(JSON.stringify(FRA(A, B)), JSON.stringify(C));
  });

  it("Matrix's size = 1091", function () {
    const [A, B] = input[5];
    const C = output[5];
    assert.equal(JSON.stringify(FRA(A, B)), JSON.stringify(C));
  }).timeout(OVERLOAD_TIMEOUT);
});
