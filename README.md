# Four Russians' Algorithm implementation

## Author: Gurin Simon

### Group: BSE 171

# What is FRA implemented on?

I used NodeJS v.12.18.1 to implement Four Russians' Algorithm

# How to install and test

1. Use `git clone <repo_uri>` in your console in order to copy this project to the directory
2. Before you run Mocha tests, you need to install NodeJS from [this source](https://nodejs.org)
3. After the NodeJS installation you can use `npm i` to install all packages you need for test run.
4. Use `npm test` to run the tests.

# What is in the repository

- `data` &mdash; directory with `input.json` as test input and `output.json` as expected output of FRA.
- `src` &mdash; folder with code sources:
  - `generator.js` used for generating test data
  - `geterateInput.js` used for generating set of input data
  - `index.js` contains FRA implementation and supporting code (like matrix transposition and scalar multiplication)
- `test` &mdash; directory with `index.test.js` where you may find testing code

> After running `npm i` you can also find `node_modules` folder. It contains packages' and libraries' folders
